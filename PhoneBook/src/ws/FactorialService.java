package ws;

public class FactorialService {

	public double factorial(int sayi) {
		try {
			int factorial = 1;
			for (int i = 1; i <= sayi; i++) {
				factorial *= i;
			}
			return factorial;

		} catch (Exception e) {

			System.err.println(e.toString());
			e.printStackTrace();
		}
		return -1.0;

	}
}
