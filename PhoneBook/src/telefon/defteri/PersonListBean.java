package telefon.defteri;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class PersonListBean {
	private List<Person> persons;
	private static String URL = "jdbc:mysql://localhost:3306/telefon_defteri";
	private static String USERNAME = "root";
	private static String PASSWORD = "pztrn_1";

	private Person currentPerson;
	private boolean showAddPanel;
	private boolean showEditPanel;

	public PersonListBean() {
		loadPerson();
		currentPerson = new Person();
		showAddPanel = false;
		setShowEditPanel(false);
	}

	public void deleteRecord() {

		System.out.println(currentPerson.toString());

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(URL, USERNAME,
					PASSWORD);
			String SQL = "DELETE  FROM `kisiler` WHERE `id`=? ";
			PreparedStatement prstmt = connection.prepareStatement(SQL);
			prstmt.setInt(1, getCurrentPerson().getId());
			prstmt.execute();
			prstmt.close();
			connection.close();
			
			for (int i = 0; i < persons.size(); i++) {
				if(persons.get(i).getId() == currentPerson.getId()){
					persons.remove(i);
				}
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void editRecord() {
		int x = 0;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(URL, USERNAME,
					PASSWORD);
			String SQL = "UPDATE `kisiler` SET `name`=?, `surname`=?, `country`=?,`city`=?,`adress`=?,`GSM`=?,`phone`=?  WHERE `id`=?";
			PreparedStatement prstmt = connection.prepareStatement(SQL);
			prstmt.setString(++x, getCurrentPerson().getName());
			prstmt.setString(++x, getCurrentPerson().getSurname());
			prstmt.setString(++x, getCurrentPerson().getCountry());
			prstmt.setString(++x, getCurrentPerson().getCity());
			prstmt.setString(++x, getCurrentPerson().getAdress());
			prstmt.setString(++x, getCurrentPerson().getGsm());
			prstmt.setString(++x, getCurrentPerson().getPhone());
			prstmt.setInt(++x, getCurrentPerson().getId());
			prstmt.execute();
			getPersons().add(getCurrentPerson());
			prstmt.close();
			connection.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void saveNewRecord() {
		System.out.println(getCurrentPerson().toString());
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(URL, USERNAME,
					PASSWORD);
			PreparedStatement prstmt = connection
					.prepareStatement("INSERT INTO kisiler(name,surname,Country,City,Adress,GSM,Phone) VALUES(?,?,?,?,?,?,?)");
			prstmt.setString(1, getCurrentPerson().getName());
			prstmt.setString(2, getCurrentPerson().getSurname());
			prstmt.setString(3, getCurrentPerson().getCountry());
			prstmt.setString(4, getCurrentPerson().getCity());
			prstmt.setString(5, getCurrentPerson().getAdress());
			prstmt.setString(6, getCurrentPerson().getGsm());
			prstmt.setString(7, getCurrentPerson().getPhone());
			prstmt.execute();
			getPersons().add(getCurrentPerson());
			prstmt.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();

		}
		System.out.println("kaydedildi...");
		showAddPanel = false;
	}

	public void cancelNewRecord() {
		System.out.println(currentPerson.toString());
		System.out.println("iptal edildi...");
		showAddPanel = false;
		currentPerson = new Person();
	}

	public void cancelEditRecord() {
		System.out.println(currentPerson.toString());
		System.out.println("iptal edildi...");
		showEditPanel = false;
		currentPerson = new Person();
	}

	public void openAddPanel() {
		currentPerson = new Person();
		setShowAddPanel(true);
	}

	public void openEditPanel() {
		setShowEditPanel(true);
		System.out.println(currentPerson.toString());
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	private void loadPerson() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(URL, USERNAME,
					PASSWORD);
			String SQL = "SELECT * FROM `kisiler`";
			PreparedStatement prstmt = connection.prepareStatement(SQL);
			ResultSet rs = prstmt.executeQuery();
			setPersons(new ArrayList<Person>());
			while (rs.next()) {
				Person person = new Person();
				person.setId(rs.getInt("id"));
				person.setName(rs.getString("name"));
				person.setSurname(rs.getString("surname"));
				person.setCountry(rs.getString("country"));
				person.setCity(rs.getString("city"));
				person.setAdress(rs.getString("adress"));
				person.setGsm(rs.getString("GSM"));
				person.setPhone(rs.getString("Phone"));
				persons.add(person);
			}
			rs.close();
			prstmt.close();
			connection.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();

		}

	}

	public Person getCurrentPerson() {
		return currentPerson;
	}

	public void setCurrentPerson(Person currentPerson) {
		this.currentPerson = currentPerson;
	}

	public boolean isShowAddPanel() {
		return showAddPanel;
	}

	public void setShowAddPanel(boolean showAddPanel) {
		this.showAddPanel = showAddPanel;
	}

	public boolean isShowEditPanel() {
		return showEditPanel;
	}

	public void setShowEditPanel(boolean showEditPanel) {
		this.showEditPanel = showEditPanel;
	}

}
